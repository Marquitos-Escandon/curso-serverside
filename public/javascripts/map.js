var map = L.map('main_map').setView([-38.00042,-57.5562], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map); 

/*
L.marker([-38.00142,-57.5662]).addTo(map);
L.marker([-38.00242,-57.5762]).addTo(map);
L.marker([-38.00342,-57.5862]).addTo(map);
*/

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicleta.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})