var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www');

describe('Testing bicicleta' , function(){

    afterEach(function(done){
        Bicicleta.deleteMany({},function(err,success){
            if(err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.toString',()=>{
        it('devuelve string correcto',()=>{
            var bici = Bicicleta.createInstance(1,"verde","urbana",[40.31,-3.7]);
            var stringCalculado = 'code: '+bici.code+' color: '+bici.color;
            var stringBici = bici.toString();
            expect (stringCalculado).toEqual(stringBici);
        });
    });

    describe('Bicicleta.createInstance',()=>{
        it('crear una instancia', ()=>{
            var bici = Bicicleta.createInstance(1,"verde","urbana",[40.31,-3.7]);

            expect (bici.code).toBe(1);
            expect (bici.color).toBe("verde");
            expect (bici.modelo).toBe("urbana");
            expect (bici.ubicacion).toEqual([40.31,-3.7]);
        });
    });


    describe('Bicicleta.allBicis',()=>{
        it('comienza vacia', (done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add',()=> {
        it('crear bici',(done)=>{
            var a = new Bicicleta({code:1,color:'blanca',modelo:'montaña'});
            Bicicleta.add(a,function(err,newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(a.code);
                    done();
                });
            });
        });
    });




    describe('Bicicleta.findByCode',()=> {
        it('Debe devolver la bici con code 1',(done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                var a = new Bicicleta({code:1,color:'blanca',modelo:'montaña'});
                Bicicleta.add(a,function(err,newBici){
                    if(err) console.log(err);

                    var b = new Bicicleta({code:2,color:'roja',modelo:'urbana'});
                    Bicicleta.add(b,function(err,newBici){
                        if(err) console.log(err);

                        Bicicleta.findByCode(1,function(err, bici){
                            expect(bici.code).toBe(a.code);
                            expect(bici.color).toBe(a.color);
                            expect(bici.modelo).toBe(a.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });


    describe('Bicicleta.removeByCode',()=> {
        it('Debe borrar la bici con code 1',(done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                var a = new Bicicleta({code:1,color:'blanca',modelo:'montaña'});
                Bicicleta.add(a,function(err,newBici){
                    if(err) console.log(err);

                    var b = new Bicicleta({code:2,color:'roja',modelo:'urbana'});
                    Bicicleta.add(b,function(err,newBici){
                        if(err) console.log(err);
                        // Borramos el code=1
                        Bicicleta.removeByCode(1, function(err, bici){
                            if (err) console.log(err);
                            // Buscamos el que acabamos de borrar
                            Bicicleta.findByCode(1,function(err, bici){
                                if (err) console.log(err);
                                // La busqueda debera devolver null
                                expect (bici).toBe(null);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });


    describe('UpdateByCode',()=>{
        it('actualiza datos bici',(done)=>{
            Bicicleta.allBicis(function(err,bicis){
                if(err) console.log(err);
                expect(bicis.length).toBe(0);
                var a = new Bicicleta({code:1,color:'blanca',modelo:'montaña'});
                Bicicleta.add(a,function(err,newBici){
                    if(err) console.log(err);
                    Bicicleta.allBicis(function(err,bicis){
                        if(err) console.log(err);
                        expect(bicis.length).toBe(1);
                        Bicicleta.findByCode(1,function(err, bici){
                            if (err) console.log(err);
                            expect(bici.color).toBe('blanca');
                            var b = new Bicicleta({code:1,color:'roja',modelo:'ciudad'});
                            Bicicleta.updateByCode(b,()=>{
                                Bicicleta.findByCode(1,function(err, bici){
                                    if (err) console.log(err);
                                    
                                    expect(bici.code).toBe(1);
                                    expect(bici.color).toBe('roja');
                                    expect(bici.modelo).toBe('ciudad');
                                    done();
                                });
                            });
                        });
                    });
                });
            });
        });
    });

});

/*
beforeEach(() => { Bicicleta.allBicis = []; });

describe('Bicicleta.allBicis', () => {
    it(`comienza vacia`, () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        
        var a = new Bicicleta(1, 'rojo', 'urbana', [-38.00045,-57.5565]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, "verde", "urbana");
        var aBici2 = new Bicicleta(2, "azul", "montaña");
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    });
});
*/