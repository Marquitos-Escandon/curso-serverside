var mongoose = require('mongoose');
var request = require('request');
var Bicicleta = require('../../models/bicicletas');
var server = require('../../bin/www');


describe('Testing Bicicleta API',()=>{
 
     afterEach(function(done){
        console.log("-------------->"+mongoose.connection.readyState);
        Bicicleta.deleteMany({},function(err,success){
            if(err) console.log(err);
            done();
        });
     });
 
    describe('GET bici',()=>{
        it('Status 200',()=>{
            request.get('http://localhost:3000/api/bicicletas',function(error,response,body){
                console.log("error:"+error);
                console.log("response:"+response);
                console.log("body:"+body);
                expect (response.statusCode).toBe(200);
                var result= JSON.parse(body);
                expect (result.bicicletas.length).toBe(0);
            });

        });
    });

    describe('POST create bici',()=>{
        it('Status 200',(done)=>{
            var headers = {'content-type':'application/json'};
            var aBici = '{"code":122, "color":"negra", "modelo": "montaña","lat":40.31,"lng": -3.7}';

            request.post({
                headers:headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            },function(error,response,body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                expect(bici.code).toBe(122);
                expect(bici.color).toBe("negra");
                expect(bici.modelo).toBe("montaña");
                expect(bici.ubicacion[0]).toBe(40.31);
                expect(bici.ubicacion[1]).toBe(-3.7);
                console.log(bici);
                done();
            });
        });
    });


    describe('POST update bici',()=>{
        it('Status 200',(done)=>{

            let headers = {'content-type':'application/json'};
            let aBici = '{"code":122, "color":"negra", "modelo": "montaña","lat":40.31,"lng": -3.7}';

            request.post({
                headers:headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            },function(error,response,body){
                expect(response.statusCode).toBe(200);

                let headers = {'content-type':'application/json'};
                let aBici2 = '{"code":122, "color":"roja", "modelo": "ciudad","lat":40.31,"lng": -3.7}';

                request.post({
                    headers:headers,
                    url: 'http://localhost:3000/api/bicicletas/update',
                    body: aBici2
                },function(error,response,body){
                    console.log("body:"+body + " response: "+response);
                    var bici = JSON.parse(body).bicicleta;
                    expect(response.statusCode).toBe(200);
                    expect(bici.color).not.toBe("negra");
                    expect(bici.color).toBe("roja");
                    done();
                });
            });           
        });
    });



    describe('DELETE bici',()=>{
        it('Status 200',(done)=>{
            var headers = {'content-type':'application/json'};
            var aBici = '{"code":1, "color":"negra", "modelo": "montaña","lat":40.31,"lng": -3.7}';

            request.post({
                headers:headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            },function(error,response,body){
                expect(response.statusCode).toBe(200);
            
                var headers = {'content-type':'application/json'};
                var aBici = '{"code":1}';
                request.delete({
                    headers:headers,
                    url: 'http://localhost:3000/api/bicicletas/delete',
                    body: aBici
                },function(error,response,body){
                    expect(response.statusCode).toBe(200);
                    done();
                });
            });
        });
    });


});